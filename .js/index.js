let fullname; //personalName
let humanage; //personalAge
let verification; //personalBio

fullname = prompt("What is your name?");
while (!/^[a-zA-Z\s]+$/.test(fullname)) {
    fullname = prompt("Check the correctness of the personal data you have entered", fullname);
}

humanage = prompt("How old are you?");
while (humanage === 0 || Number(isNaN(humanage))) {
    humanage = prompt("How old are you?", humanage);
}

if (humanage < 18) {
    alert("You are not allowed to visit this website");
} else if (humanage >= 18 && humanage <= 22) {
    verification = confirm("Are you sure you want to continue?");
    if (verification) { 
        alert("Welcome " + fullname);
    } else { 
        alert("You are not allowed to visit this website");
    }
} else { 
    alert("Welcome " + fullname);
}


