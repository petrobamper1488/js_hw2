### Які існують типи даних у Javascript? ###

В JavaScript існують такі типи данних: *рядкові, числові, булеві, масиви, об'єктні, null, undefined*«.»
Кожен з них має свої властивості і сферу застосування«.»

### У чому різниця між == і ===? ###

**==** це оператор порівняння нестрогої рівністі, від **===** (сувора рівність) суттєво відрізняється тим що не перетворює типи данних«.» Бажано використовувати в проектах саме **===**, аби уникати значної кількості помилок в роботі коду«.»

### Що таке оператор? ###

**Оператор** - це символьна послідовність в JavaScript, яка може виконувати цілий ряд дій зі значеннями, змінними ітд«.»
